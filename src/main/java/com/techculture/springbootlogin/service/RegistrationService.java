package com.techculture.springbootlogin.service;

import com.techculture.springbootlogin.dto.UserDto;
import com.techculture.springbootlogin.domain.User;
import com.techculture.springbootlogin.domain.UserRole;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@AllArgsConstructor
public class RegistrationService {
    private final UserService service;
    private final EmailValidator emailValidator;

    public String register(UserDto request) {
        boolean isValid = emailValidator.test(request.getEmail());
        if(!isValid) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email not valid");
        }
        return service.signUpUser(
                new User(
                        request.getFirstName(),
                        request.getLastName(),
                        request.getEmail(),
                        request.getPassword(),
                        UserRole.USER
                )
        );
    }
}
