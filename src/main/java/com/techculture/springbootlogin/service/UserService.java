package com.techculture.springbootlogin.service;

import com.techculture.springbootlogin.domain.User;
import com.techculture.springbootlogin.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository repo;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return repo.findByEmail(s).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email not found."));
    }

    public String signUpUser(User u) {
        boolean userExists = repo.findByEmail(u.getEmail()).isPresent();
        if(userExists) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already registered");
        }

        String encodedPassword = passwordEncoder.encode(u.getPassword());
        u.setPassword(encodedPassword);
        repo.save(u);
        return "User registered successfully";
    }
}
