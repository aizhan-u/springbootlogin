package com.techculture.springbootlogin.controller;

import com.techculture.springbootlogin.dto.UserDto;
import com.techculture.springbootlogin.service.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/registration")
@AllArgsConstructor
public class UserRegistrationController {
    private final RegistrationService service;

    @PostMapping
    public String registerUserAccount(@RequestBody UserDto request) {
        return service.register(request);
    }
}
