package com.techculture.springbootlogin.controller;

import com.techculture.springbootlogin.jwt.JwtToken;
import com.techculture.springbootlogin.service.UserService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/token")
@Setter
@Getter
@AllArgsConstructor
public class TokenController {
    private final JwtToken jwtToken;
    private final UserService userService;

    @PostMapping("/check")
    public String checkToken(@RequestParam String token) {
        if(jwtToken.validateToken(token)) {
            return "Correct token";
        }
        return "Token error";
    }
}
