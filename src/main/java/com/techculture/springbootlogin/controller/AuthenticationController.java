package com.techculture.springbootlogin.controller;

import com.techculture.springbootlogin.domain.User;
import com.techculture.springbootlogin.dto.AuthDto;
import com.techculture.springbootlogin.jwt.JwtToken;
import com.techculture.springbootlogin.service.UserService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
@Setter
@Getter
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final JwtToken jwtToken;
    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody AuthDto authDto) {
        try {
            String username = authDto.getEmail();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, authDto.getPassword()));
            UserDetails user = userService.loadUserByUsername(username);
            if(user == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username not found");
            }

            String token = jwtToken.createToken(username);
            Map<Object, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid username or password");
        }
    }
}
