package com.techculture.springbootlogin.domain;

public enum UserRole {
    USER,
    ADMIN
}
