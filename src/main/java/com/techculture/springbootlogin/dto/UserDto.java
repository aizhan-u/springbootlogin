package com.techculture.springbootlogin.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UserDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
